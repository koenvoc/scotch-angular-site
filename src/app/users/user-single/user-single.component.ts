import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UserServiceService } from "src/app/user-service.service";
@Component({
  selector: "app-user-single",
  template: `
    <section class="section">
      <div class="container">
        <div class="card" *ngIf="user">
          <img [src]="user.avatar_url" />
          <h2>{{ user.login }}</h2>
        </div>
      </div>
    </section>
  `,
  styles: [],
})
export class UserSingleComponent implements OnInit {
  user;
  constructor(
    private route: ActivatedRoute,
    private userService: UserServiceService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      // get the username out of the route params
      const username = params["username"];
      this.userService
        .getUser(username)
        .subscribe((user) => (this.user = user));
    });
  }
}
