import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../../user-service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-list',
  template: `
   <div class="card" *ngFor="let user of users | async">
   <a routerLink="/users/{{ user.login }}">{{ user.login }}</a>
    </div>
  `,
  styles: [
  ]
})
export class UserListComponent implements OnInit {
users
  constructor(private userService: UserServiceService) { }

  ngOnInit(): void {
    this.users = this.userService.getUsers();
  }

}
