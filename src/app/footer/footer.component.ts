import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
   <!--Footer-->
   <footer class="footer ">
    <div class="container content has-text-centered">
      <p>Made with 💕  by chris form scotch </p>
    </div>
    </footer>
  `,
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
